package com.shawarmadrop.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import static com.shawarmadrop.game.SizeFactor.MAX_VIEW_WIDTH;

public class InitFont {
    private static final String INTERNAL_FONT_PATH = "font/verdana_font.fnt";
    public static BitmapFont getInitFont() {
        BitmapFont bitmapFont = new BitmapFont(Gdx.files.internal(INTERNAL_FONT_PATH));
        bitmapFont.getData().setScale(MAX_VIEW_WIDTH / 1000f);
        return bitmapFont;
    }
}