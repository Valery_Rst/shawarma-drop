package com.shawarmadrop.game;

public interface SizeFactor {
    float MAX_VIEW_HEIGHT = 480;
    float MAX_VIEW_WIDTH = 800;
    float CART_HEIGHT_SIZE = 83;
    float CART_WIDTH_SIZE = 90;
    float CART_LOWER_LIMIT_FACTOR = 15;
    float SHAWARMA_HEIGHT_SIZE = 64;
    float SHAWARMA_WIDTH_SIZE = 34;
}