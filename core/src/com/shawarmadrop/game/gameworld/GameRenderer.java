package com.shawarmadrop.game.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shawarmadrop.game.SizeFactor;
import com.shawarmadrop.game.controller.ScreenElementController;
import com.shawarmadrop.game.util.InitFont;

public class GameRenderer implements SizeFactor {

    private GameWorld gameWorld;
    private BitmapFont bitmapFont;

    public GameRenderer(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        bitmapFont = InitFont.getInitFont();
    }

    public void draw(SpriteBatch spriteBatch) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        for (ScreenElementController service : gameWorld.getControllersList()) {
            service.drawElement(spriteBatch);
        }
        if (gameWorld.isReady()) {
            bitmapFont.draw(spriteBatch, "Ready? Do touch! ", MAX_VIEW_WIDTH / 2.7f, MAX_VIEW_HEIGHT / 1.6f);
            onTouchChangeGameState();
        } else if (gameWorld.isGameOver()) {
            drawMaxScore(spriteBatch);
            bitmapFont.draw(spriteBatch, "Game over! \nDo touch to play! ", MAX_VIEW_WIDTH / 2.7f, MAX_VIEW_HEIGHT / 1.6f);
            onTouchChangeGameState();
        } else if (gameWorld.isStart()) {
            drawScore(spriteBatch);
        }
        spriteBatch.end();
    }

    public void dispose() {
        bitmapFont.dispose();
        for (ScreenElementController service : gameWorld.getControllersList()) {
            service.disposeElement();
        }
    }

    private void drawScore(SpriteBatch spriteBatch) {
        drawMaxScore(spriteBatch);
        bitmapFont.draw(spriteBatch, "Score of received: " + gameWorld.getReceivedScore(), MAX_VIEW_WIDTH / 15.5f, MAX_VIEW_HEIGHT / 1.1f);
        bitmapFont.draw(spriteBatch, "Score of lost: " + gameWorld.getLostScore(), MAX_VIEW_WIDTH / 15.5f, MAX_VIEW_HEIGHT / 1.2f);
    }

    private void drawMaxScore(SpriteBatch spriteBatch) {
        bitmapFont.draw(spriteBatch, "Max of received score: " + gameWorld.getMaxScore(), MAX_VIEW_WIDTH / 1.54f, MAX_VIEW_HEIGHT / 1.1f);
    }

    private void onTouchChangeGameState() {
        if (isTouched()) gameWorld.doReformatGameState();
    }

    private boolean isTouched() {
        return Gdx.input.isTouched();
    }
}