package com.shawarmadrop.game.gameworld;

public enum GameState {
    READY, START, GAME_OVER
}