package com.shawarmadrop.game.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.shawarmadrop.game.SizeFactor;
import com.shawarmadrop.game.controller.BackgroundElementController;
import com.shawarmadrop.game.controller.CartElementController;
import com.shawarmadrop.game.controller.ScreenElementController;
import com.shawarmadrop.game.controller.ShawarmaElementController;
import com.shawarmadrop.game.model.BackgroundPicture;
import com.shawarmadrop.game.model.Cart;
import com.shawarmadrop.game.model.Shawarma;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameWorld implements SizeFactor {

    private static final String INTERNAL_RECEIVED_SOUND_PATH = "sound/received_drop.wav";
    private static final String INTERNAL_LOST_SOUND_PATH = "sound/lost_drop.wav";

    private BackgroundPicture backgroundPicture;
    private Cart cart;
    private Array<Shawarma> shawarmaArray;
    private Pool<Shawarma> shawarmaPool = Pools.get(Shawarma.class);

    private Sound receivedSound, lostSound;
    private CartElementController cartElementController;
    private ShawarmaElementController shawarmaElementController;
    private List<ScreenElementController> controllersList;

    private int receivedScore, lostScore, maxScore;
    private Preferences preferences;
    private GameState gameState;
    public float delta;

    public GameWorld() {
        gameState = GameState.READY;
        shawarmaArray = new Array<>();

        backgroundPicture = new BackgroundPicture();
        cart = new Cart(MAX_VIEW_WIDTH / 2 - CART_WIDTH_SIZE / 2, CART_LOWER_LIMIT_FACTOR, CART_WIDTH_SIZE, CART_HEIGHT_SIZE);
        cartElementController = new CartElementController(this);
        shawarmaElementController = new ShawarmaElementController(this);
        controllersList = new ArrayList<>(Arrays.asList(
                new BackgroundElementController(this),
                cartElementController, shawarmaElementController));

        shawarmaArray.add(shawarmaElementController.getInitShawarmaFromPoll());
        receivedSound = Gdx.audio.newSound(Gdx.files.internal(INTERNAL_RECEIVED_SOUND_PATH));
        lostSound = Gdx.audio.newSound(Gdx.files.internal(INTERNAL_LOST_SOUND_PATH));
        receivedScore = 0;
        lostScore = 0;
        preferences = Gdx.app.getPreferences("maxScore");
        maxScore = preferences.getInteger("maxScore");
    }

    public Cart getCart() {
        return cart;
    }
    public Pool<Shawarma> getShawarmaPool() {
        return shawarmaPool;
    }
    public Array<Shawarma> getShawarmaArray() {
        return shawarmaArray;
    }
    public BackgroundPicture getBackgroundPicture() {
        return backgroundPicture;
    }
    int getReceivedScore() {
        return receivedScore;
    }
    int getLostScore() {
        return lostScore;
    }
    int getMaxScore() {
        return maxScore;
    }
    List<ScreenElementController> getControllersList() {
        return controllersList;
    }

    public void update(OrthographicCamera camera, float delta) {
        this.delta = delta;
        for (ScreenElementController service : getControllersList()) {
            service.updateElement(camera);
        }
        if (isStart()) shawarmaDropHandle(delta, shawarmaElementController.increaseSpeedDropSpawn());
        checkingOverGame();
    }

    private void shawarmaDropHandle(float delta, float speed) {
        for (Shawarma shawarma : shawarmaArray) {
            shawarma.y -= speed * delta;
            if (isLowerLimitFactor(shawarma)) {
                shawarma.setFallen(false);
                lostSound.play();
                addLostScore();
            }
            if (isOverlapsFactor(shawarma, cart)) {
                shawarma.setFallen(false);
                receivedSound.play();
                addReceivedScore();
                updateMaxScore();
            }
        }
    }

    private boolean isOverlapsFactor(Shawarma shawarma, Cart cart) {
        return shawarma.x < (getCartBeginPositionFactor() + getCartEndPositionFactor()) && shawarma.x + shawarma.width > getCartBeginPositionFactor()
                && shawarma.y < cart.y + cart.height - 20 && shawarma.y + shawarma.height > cart.y;
    }
    private float getCartBeginPositionFactor() {
        return this.cart.x + 36;
    }
    private float getCartEndPositionFactor() {
        return this.cart.width - 56;
    }
    private boolean isLowerLimitFactor(Shawarma shawarma) {
        return shawarma.y + SHAWARMA_HEIGHT_SIZE < 0;
    }

    private void addLostScore() {
        this.lostScore += 1;
    }
    private void addReceivedScore() {
        this.receivedScore += 1;
    }
    private void updateMaxScore() {
        if (this.receivedScore > this.maxScore) {
            this.maxScore = this.receivedScore;
        }
    }
    private void saveMaxScore() {
        preferences.putInteger("maxScore", getMaxScore());
        preferences.flush();
    }

    private void checkingOverGame() {
        if (isOverGameScore()) {
            gameState = GameState.GAME_OVER;
        }
    }
    void doReformatGameState() {
        switch (gameState){
            case GAME_OVER:
                restart();
                break;
            case READY:
                start();
                break;
        }
    }

    public boolean isStart() {
        return gameState == GameState.START;
    }
    boolean isReady() {
        return gameState == GameState.READY;
    }
    boolean isGameOver() {
        return gameState == GameState.GAME_OVER;
    }

    private void start() {
        gameState = GameState.START;
    }
    private void restart() {
        receivedScore = 0;
        lostScore = 0;
        shawarmaElementController.onRestart();
        cartElementController.onRestart();
        gameState = GameState.READY;
        saveMaxScore();
    }

    private boolean isOverGameScore() {
        return this.lostScore > 9;
    }

    public void dispose() {
        saveMaxScore();
        lostSound.dispose();
        receivedSound.dispose();
    }
}