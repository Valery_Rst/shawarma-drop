package com.shawarmadrop.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shawarmadrop.game.screen.LaunchScreen;

public class ShawarmaDropGame extends Game implements SizeFactor {

    private SpriteBatch spriteBatch;
    private OrthographicCamera camera;

    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }
    public OrthographicCamera getCamera() {
        return camera;
    }

    public void create() {
        spriteBatch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT);
        this.setScreen(new LaunchScreen(this));
    }

    public void dispose() {
        super.dispose();
        spriteBatch.dispose();
    }
}