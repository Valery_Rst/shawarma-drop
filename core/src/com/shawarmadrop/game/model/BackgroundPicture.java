package com.shawarmadrop.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class BackgroundPicture {

    private static final String INTERNAL_PICTURE_PATH = "img/background.png";
    private Texture texture;
    private Vector2 vector;

    public BackgroundPicture() {
        texture = new Texture(INTERNAL_PICTURE_PATH);
        vector = new Vector2(0, 0);
    }

    public Texture getTexture() {
        return texture;
    }
    public Vector2 getVector() {
        return vector;
    }
}