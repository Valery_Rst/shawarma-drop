package com.shawarmadrop.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class Shawarma extends Rectangle {

    private static final String INTERNAL_PICTURE_PATH = "img/shawarma.png";
    private Texture texture;
    private boolean fallen;

    public Shawarma () {}

    public boolean isFallen() {
        return fallen;
    }

    public void setFallen(boolean fallen) {
        this.fallen = fallen;
    }

    public Texture getTexture() {
        return texture;
    }

    public void init(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.texture = new Texture(INTERNAL_PICTURE_PATH);
        this.fallen = true;
    }
}