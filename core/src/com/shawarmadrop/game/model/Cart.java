package com.shawarmadrop.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class Cart extends Rectangle {

    private static final String INTERNAL_PICTURE_PATH = "img/cart.png";
    private float startX, startY, startWidth, startHeight;
    private Texture texture;

    public Cart(float x, float y, float width, float height){
        this.x = startX = x;
        this.y = startY = y;
        this.width = startWidth = width;
        this.height = startHeight = height;
        this.texture = new Texture(INTERNAL_PICTURE_PATH);
    }

    public Texture getTexture() {
        return texture;
    }

    public void resetPosition() {
        this.x = startX;
        this.y = startY;
        this.width = startWidth;
        this.height = startHeight;
    }
}