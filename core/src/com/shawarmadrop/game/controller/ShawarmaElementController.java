package com.shawarmadrop.game.controller;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.shawarmadrop.game.SizeFactor;
import com.shawarmadrop.game.gameworld.GameWorld;
import com.shawarmadrop.game.model.Shawarma;

public class ShawarmaElementController implements ScreenElementController, SizeFactor {

    private static final float MAX_SPEED_SPAWN_DROP = 80.5f;
    private static final double MAX_INCREMENT = 0.005;
    private static final float MIN_DELAY_TIME = 2.0f;

    private GameWorld gameWorld;
    private Array<Shawarma> shawarmaArray;
    private Pool<Shawarma> shawarmaPool;
    private float speedDrop, delayTime, delayCounter;

    public ShawarmaElementController(GameWorld gameWorld){
        this.gameWorld = gameWorld;
        this.shawarmaPool = gameWorld.getShawarmaPool();
        this.shawarmaArray = gameWorld.getShawarmaArray();
        this.speedDrop = 50;
        this.delayTime = 18.5f;
        this.delayCounter = 0.0f;
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        for (Shawarma shawarma : shawarmaArray) {
            spriteBatch.draw(shawarma.getTexture(), shawarma.getX(), shawarma.getY());
        }
    }

    @Override
    public void updateElement(OrthographicCamera camera) {
        if (gameWorld.isStart()) {
            if(delayCounter >= decreaseDelayTime()){
                shawarmaArray.add(getInitShawarmaFromPoll());
                delayCounter = 0.0f;
            } else{
                delayCounter += gameWorld.delta;
            }
            freeFallingItems();
        }
    }

    @Override
    public void disposeElement() {
        for (Shawarma shawarma : shawarmaArray) {
            shawarma.getTexture().dispose();
        }
    }

    public float increaseSpeedDropSpawn() {
        if (this.speedDrop < MAX_SPEED_SPAWN_DROP) this.speedDrop += MAX_INCREMENT;
        return this.speedDrop;
    }

    private float decreaseDelayTime() {
        if (this.delayTime > MIN_DELAY_TIME) this.delayTime -= MAX_INCREMENT;
        return this.delayTime;
    }

    public void onRestart() {
        this.shawarmaPool.freeAll(shawarmaArray);
        this.shawarmaArray.clear();
        this.speedDrop = 50;
        this.delayTime = 18.5f;
        this.delayCounter = 0.0f;
    }

    public Shawarma getInitShawarmaFromPoll() {
        Shawarma shawarma = shawarmaPool.obtain();
        shawarma.init(MathUtils.random(0, MAX_VIEW_WIDTH - SHAWARMA_WIDTH_SIZE),
                MAX_VIEW_HEIGHT, SHAWARMA_WIDTH_SIZE, SHAWARMA_HEIGHT_SIZE);
        return shawarma;
    }

    private void freeFallingItems() {
        Shawarma shawarma;
        for (int i = shawarmaArray.size; --i >= 0;) {
            shawarma = shawarmaArray.get(i);
            if (!shawarma.isFallen()) {
                shawarmaArray.removeIndex(i);
                shawarmaPool.free(shawarma);
            }
        }
    }
}