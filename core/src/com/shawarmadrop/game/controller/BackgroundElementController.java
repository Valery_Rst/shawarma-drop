package com.shawarmadrop.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shawarmadrop.game.gameworld.GameWorld;
import com.shawarmadrop.game.model.BackgroundPicture;

public class BackgroundElementController implements ScreenElementController {

    private static final String INTERNAL_MUSIC_PATH = "music/joy.mp3";
    private static Music backgroundMusic;
    private BackgroundPicture backgroundPicture;

    public BackgroundElementController() {
        backgroundPicture = new BackgroundPicture();
        playLoopingMusic();
    }
    public BackgroundElementController(GameWorld gameWorld) {
        backgroundPicture = gameWorld.getBackgroundPicture();
        playLoopingMusic();
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        spriteBatch.draw(backgroundPicture.getTexture(), backgroundPicture.getVector().x, backgroundPicture.getVector().y);
    }

    @Override
    public void updateElement(OrthographicCamera camera) {}

    @Override
    public void disposeElement() {
        backgroundPicture.getTexture().dispose();
        backgroundMusic.dispose();
    }

    private void playLoopingMusic() {
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal(INTERNAL_MUSIC_PATH));
        backgroundMusic.setLooping(true);
        backgroundMusic.play();
    }
}