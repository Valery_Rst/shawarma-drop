package com.shawarmadrop.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.shawarmadrop.game.SizeFactor;
import com.shawarmadrop.game.gameworld.GameWorld;
import com.shawarmadrop.game.model.Cart;

public class CartElementController implements ScreenElementController, SizeFactor {

    private GameWorld gameWorld;
    private Cart cart;
    private Vector3 touchPosition;

    public CartElementController(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        this.cart = gameWorld.getCart();
        this.touchPosition = new Vector3();
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        spriteBatch.draw(cart.getTexture(), cart.x, cart.y);
    }

    @Override
    public void updateElement(OrthographicCamera camera) {
        if(Gdx.input.isTouched()) {
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPosition);
            cart.x = (touchPosition.x - CART_WIDTH_SIZE / 2);
        }

        if(cart.x < 0) cart.x = 0;
        if(cart.x > MAX_VIEW_WIDTH - CART_WIDTH_SIZE) cart.x = MAX_VIEW_WIDTH - CART_WIDTH_SIZE;
    }

    @Override
    public void disposeElement() {
        cart.getTexture().dispose();
    }

    public void onRestart() {
        gameWorld.getCart().resetPosition();
    }
}