package com.shawarmadrop.game.controller;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface ScreenElementController {
    void drawElement(SpriteBatch spriteBatch);
    void updateElement(OrthographicCamera camera);
    void disposeElement();
}