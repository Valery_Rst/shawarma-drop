package com.shawarmadrop.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;
import com.shawarmadrop.game.SizeFactor;
import com.shawarmadrop.game.ShawarmaDropGame;
import com.shawarmadrop.game.controller.BackgroundElementController;
import com.shawarmadrop.game.util.InitFont;

public class LaunchScreen implements Screen, SizeFactor {

    private static final String INTERNAL_PICTURE_PATH = "img/play_button.png";
    private static final float PLAY_BTN_HEIGHT_SIZE = 100;
    private static final float PLAY_BTN_WIDTH_SIZE = 100;

    private ShawarmaDropGame shawarmaDropGame;
    private BackgroundElementController backgroundElementOrganizer;
    private BitmapFont bitmapFont;
    private Texture startButtonTexture;
    private Sprite startButtonSprite;
    private Vector3 tempCoordinates;

    public LaunchScreen(ShawarmaDropGame shawarmaDropGame) {
        this.shawarmaDropGame = shawarmaDropGame;
        backgroundElementOrganizer = new BackgroundElementController();
        bitmapFont = InitFont.getInitFont();
        startButtonTexture = new Texture(Gdx.files.internal(INTERNAL_PICTURE_PATH));
        startButtonSprite = new Sprite(startButtonTexture);
        startButtonSprite.setSize(PLAY_BTN_WIDTH_SIZE, PLAY_BTN_HEIGHT_SIZE);
        startButtonSprite.setPosition(MAX_VIEW_WIDTH / 2.4f , MAX_VIEW_HEIGHT / 3.5f);
        tempCoordinates = new Vector3();
    }

    @Override
    public void show() {}

    @Override
    public void render(float delta) {
        shawarmaDropGame.getCamera().update();
        shawarmaDropGame.getSpriteBatch().setProjectionMatrix(shawarmaDropGame.getCamera().combined);
        shawarmaDropGame.getSpriteBatch().begin();
        backgroundElementOrganizer.drawElement(shawarmaDropGame.getSpriteBatch());
        startButtonSprite.draw(shawarmaDropGame.getSpriteBatch());
        bitmapFont.draw(shawarmaDropGame.getSpriteBatch(), "Welcome to Shawarma Drop! ", MAX_VIEW_WIDTH / 2.9f, MAX_VIEW_HEIGHT / 1.6f);
        bitmapFont.draw(shawarmaDropGame.getSpriteBatch(),"Tap on play button to begin! ", MAX_VIEW_WIDTH / 2.9f, MAX_VIEW_HEIGHT / 1.7f);
        shawarmaDropGame.getSpriteBatch().end();
        handleTouch();
    }

    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        startButtonTexture.dispose();
        bitmapFont.dispose();
        backgroundElementOrganizer.disposeElement();
    }

    private void handleTouch() {
        if (Gdx.input.justTouched()) {
            tempCoordinates.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            shawarmaDropGame.getCamera().unproject(tempCoordinates);
            float touchX = tempCoordinates.x;
            float touchY = tempCoordinates.y;
            if((touchX >= startButtonSprite.getX()) && touchX <= (startButtonSprite.getX() + startButtonSprite.getWidth())
                    && (touchY >= startButtonSprite.getY()) && touchY <= (startButtonSprite.getY() + startButtonSprite.getHeight()) ){
                shawarmaDropGame.setScreen(new GameScreen(shawarmaDropGame));
            }
        }
    }
}