package com.shawarmadrop.game.screen;

import com.badlogic.gdx.Screen;
import com.shawarmadrop.game.SizeFactor;
import com.shawarmadrop.game.gameworld.GameWorld;
import com.shawarmadrop.game.gameworld.GameRenderer;
import com.shawarmadrop.game.ShawarmaDropGame;

public class GameScreen implements Screen, SizeFactor {

	private ShawarmaDropGame shawarmaDropGame;
	private GameRenderer gameRenderer;
	private GameWorld gameWorld;

	GameScreen(ShawarmaDropGame shawarmaDropGame) {
		this.shawarmaDropGame = shawarmaDropGame;
		gameWorld = new GameWorld();
		gameRenderer = new GameRenderer(gameWorld);
	}

	@Override
	public void render(float delta) {
		shawarmaDropGame.getCamera().update();
		shawarmaDropGame.getSpriteBatch().setProjectionMatrix(shawarmaDropGame.getCamera().combined);
        gameRenderer.draw(shawarmaDropGame.getSpriteBatch());
        gameWorld.update(shawarmaDropGame.getCamera(), delta);
	}

	@Override
	public void dispose() {
		gameRenderer.dispose();
		gameWorld.dispose();
	}

	@Override
	public void show() {}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {
		dispose();
	}
}